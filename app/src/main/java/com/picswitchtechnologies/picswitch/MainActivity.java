package com.picswitchtechnologies.picswitch;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Rect;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.*;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ViewFlipper;
import com.backendless.Backendless;
import com.backendless.BackendlessUser;
import com.backendless.async.callback.BackendlessCallback;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.games.Games;
import com.google.android.gms.games.GamesActivityResultCodes;
import com.google.android.gms.games.GamesStatusCodes;
import com.google.android.gms.games.multiplayer.Invitation;
import com.google.android.gms.games.multiplayer.Multiplayer;
import com.google.android.gms.games.multiplayer.OnInvitationReceivedListener;
import com.google.android.gms.games.multiplayer.Participant;
import com.google.android.gms.games.multiplayer.realtime.*;
import com.google.example.games.basegameutils.BaseGameUtils;
import org.apache.http.HttpEntityEnclosingRequest;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpRequestRetryHandler;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.ExecutionContext;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;

import javax.net.ssl.SSLException;
import java.io.*;
import java.net.ConnectException;
import java.net.MalformedURLException;
import java.net.UnknownHostException;
import java.util.*;

public class MainActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
        View.OnClickListener, RealTimeMessageReceivedListener,
        RoomStatusUpdateListener, RoomUpdateListener, OnInvitationReceivedListener {

    String CHECK_LOG_IN = "id";
    String mUserID;

    Button signInButton;
    /* Client used to interact with Google APIs. */
    private GoogleApiClient mGoogleApiClient;

    private boolean mResolvingConnectionFailure = false;
    private boolean mAutoStartSignInFlow = true;
    private boolean mSignInClicked = false;
    final static String TAG = "PicSwitch";
    // Room ID where the currently active game is taking place; null if we're
    // not playing.
    String mRoomId = null;

    //current screen id
    int mCurScreen = -1;
    // The participants in the currently active game
    ArrayList<Participant> mParticipants = null;

    // My participant ID in the currently active game
    String mMyId = null;

    // If non-null, this is the id of the invitation we received via the
    // invitation listener
    String mIncomingInvitationId = null;
    // This array lists everything that's clickable, so we can install click
// event handlers.
    final static int[] CLICKABLES = {
            R.id.button_quick_game,
            R.id.button_sign_in,
            R.id.button_quick_game,
            //R.id.button_sign_out,
            R.id.button_new_game
    };

    // This array lists all the individual screens our game has.
    final static int[] SCREENS = {
            R.id.game_screen, R.id.screen_main, R.id.screen_sign_in,
            R.id.screen_wait, R.id.screen_view_puzzle
    };
    // at least 2 players required for our game
    final static int MIN_PLAYERS = 1, MIN_OPPONENTS = 1, MAX_OPPONENTS = 1;
    // are we already playing?
    boolean mPlaying = false;

    private static final int RC_SIGN_IN = 9001;
    final static int RC_WAITING_ROOM = 10002;


    //ViewFlipper for animation from Game Screen to View Puzzle
    ViewFlipper viewFlipper;
    Animation slide_in_left, slide_out_right;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //setupWindowAnimations();
        String appVersion = "v1";
        Backendless.initApp(this, "723238F7-E6F7-1228-FF28-0259B6672700", "DF16D3B2-F0C6-71FC-FF5D-5FE832DEB000", appVersion);
        checkLogIn();
        // Create the google api Client with access to games
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(Games.API).addScope(Games.SCOPE_GAMES)
                .setViewForPopups(findViewById(android.R.id.content))
                .build();

        // set up a click listener for everything we care about
        for (int id : CLICKABLES) {
            findViewById(id).setOnClickListener(this);
        }


        //ViewFlipper
        viewFlipper = (ViewFlipper) findViewById(R.id.viewflipper);

        slide_in_left = AnimationUtils.loadAnimation(this,
                android.R.anim.slide_in_left);
        slide_out_right = AnimationUtils.loadAnimation(this,
                android.R.anim.slide_out_right);

        viewFlipper.setInAnimation(slide_in_left);
        viewFlipper.setOutAnimation(slide_out_right);


    }

    //create user or check to see if phone already registered
    public void checkLogIn() {
        //get shared preferences
        SharedPreferences sharedPref = getPreferences(Context.MODE_PRIVATE);

        //check for existing id
        mUserID = sharedPref.getString(CHECK_LOG_IN, "");

        System.out.println("mUserID outside = " + mUserID);

        //if no existing id, create new one, then save it, create new user
        if (mUserID.equals("")) {
            mUserID = UUID.randomUUID().toString();
            System.out.println("mUserID inside = " + mUserID);
            SharedPreferences.Editor mEdit = sharedPref.edit();
            mEdit.putString(CHECK_LOG_IN, mUserID);
            mEdit.commit();

            //create user
            BackendlessUser mUser = new BackendlessUser();
            mUser.setProperty("deviceID", mUserID);
            mUser.setPassword(mUserID);
            Backendless.UserService.register(mUser, new BackendlessCallback<BackendlessUser>() {
                @Override
                public void handleResponse(BackendlessUser response) {
                    System.out.println("Successfully registered user " + response.getProperty("deviceId"));
                    loginHelper();
                }
            });
        } else {
            loginHelper();
        }
    }

    //actually log in
    public void loginHelper() {
        System.out.println("login password and username: " + mUserID);
        Backendless.UserService.login(mUserID, mUserID, new BackendlessCallback<BackendlessUser>() {
            @Override
            public void handleResponse(BackendlessUser response) {
                //open next activity
//                Intent intent = new Intent(getBaseContext(),
//                        com.picswitchtechnologies.picswitch.HomeActivity.class);
//                startActivity(intent);
//                MainActivity.this.finish();
            }
        });
    }


////////////////////MultiPlayer Methods/////////////////////


    @Override
    protected void onStart() {
        System.out.println("THis is the ID: " + R.id.screen_wait);
        switchToScreen(R.id.screen_wait);


        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            Log.w(TAG,
                    "GameHelper: client was already connected on onStart()");
        } else {
            Log.d(TAG, "Connecting client.");
            mGoogleApiClient.connect();
        }
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        // if we're in a room, leave it???? We switch to gameplay activity,
        // don't see a need for this yet
//        leaveRoom();
//
//        // stop trying to keep the screen on
//        stopKeepingScreenOn();
//        mGoogleApiClient.disconnect();
//        switchToMainScreen();
    }

    @Override
    public void onConnected(Bundle connectionHint) {
        Log.d(TAG, "onConnected() called. Sign in successful!");

        Log.d(TAG, "Sign-in succeeded.");

        // register listener so we are notified if we receive an invitation to play
        // while we are in the game
        Games.Invitations.registerInvitationListener(mGoogleApiClient, this);

        if (connectionHint != null) {
            Log.d(TAG, "onConnected: connection hint provided. Checking for invite.");
            Invitation inv = connectionHint
                    .getParcelable(Multiplayer.EXTRA_INVITATION);
            if (inv != null && inv.getInvitationId() != null) {
                // retrieve and cache the invitation ID
                Log.d(TAG, "onConnected: connection hint has a room invite!");
                acceptInviteToRoom(inv.getInvitationId());
                return;
            }
        }
        switchToMainScreen();
    }

    @Override
    public void onConnectionSuspended(int i) {
        // Attempt to reconnect
        mGoogleApiClient.connect();

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        if (mResolvingConnectionFailure) {
            // already resolving
            return;
        }

        // if the sign-in button was clicked or if auto sign-in is enabled,
        // launch the sign-in flow

        if (mSignInClicked || mAutoStartSignInFlow) {
            mAutoStartSignInFlow = false;
            mSignInClicked = false;
            mResolvingConnectionFailure = true;

            // Attempt to resolve the connection failure using BaseGameUtils.
            // The R.string.signin_other_error value should reference a generic
            // error string in your strings.xml file, such as "There was
            // an issue with sign-in, please try again later."
            if (!BaseGameUtils.resolveConnectionFailure(this,
                    mGoogleApiClient, connectionResult,
                    RC_SIGN_IN, getString(R.string.signin_other_error))) {
                mResolvingConnectionFailure = false;
            }
        }

        // Put code here to display the sign-in button
    }


    @Override
    public void onClick(View v) {
        //switchToScreen(R.id.screen_wait);
        switch (v.getId()) {
            case R.id.button_quick_game:
                // user wants to play against a random opponent right now
                startQuickGame();
                System.out.println("Quick game pressed");

                break;
//            case R.id.button_sign_out:
//                // user wants to play against a random opponent right now
//                signOutclicked();
//                break;
            case R.id.button_sign_in:
                // user wants to play against a random opponent right now
                signInClicked();
                break;
            case R.id.button_new_game:
                // user wants to play against a random opponent right now
                startGame();
                break;
        }
    }

    void startQuickGame() {
        switchToScreen(R.id.screen_wait);
        // quick-start a game with 1 randomly selected opponent
        Bundle autoMatchCriteria = RoomConfig.createAutoMatchCriteria(MIN_OPPONENTS,
                MAX_OPPONENTS, 0);
        RoomConfig.Builder rtmConfigBuilder = RoomConfig.builder(this);
        rtmConfigBuilder.setMessageReceivedListener(this);
        rtmConfigBuilder.setRoomStatusUpdateListener(this);
        rtmConfigBuilder.setAutoMatchCriteria(autoMatchCriteria);


        keepScreenOn();

        Games.RealTimeMultiplayer.create(mGoogleApiClient, rtmConfigBuilder.build());
        //startGame();
    }

    // Start the gameplay phase of the game.
    void startGame() {

        //shuffle the images array.
        Collections.shuffle(images);

        imageSetupHelper();
    }

    // Reset game variables in preparation for a new game.
//    void resetGameVars() {
//        mSecondsLeft = GAME_DURATION;
//        mScore = 0;
//        mParticipantScore.clear();
//        mFinishedParticipants.clear();
//    }

    void switchToScreen(int screenId) {
        // make the requested screen visible; hide all others.
        for (int id : SCREENS) {
            findViewById(id).setVisibility(screenId == id ? View.VISIBLE : View.GONE);

        }
        mCurScreen = screenId;
    }


    @Override
    protected void onActivityResult(int requestCode, int responseCode,
                                    Intent intent) {
        super.onActivityResult(requestCode, responseCode, intent);
        switch (requestCode) {
            case RC_WAITING_ROOM:
                // we got the result from the "waiting room" UI.
                if (responseCode == Activity.RESULT_OK) {
                    // ready to start playing
                    Log.d(TAG, "Starting game (waiting room returned OK).");
                    startGame();
                } else if (responseCode == GamesActivityResultCodes.RESULT_LEFT_ROOM) {
                    // player indicated that they want to leave the room
                    Log.d(TAG, "Result left room).");
                    leaveRoom();
                } else if (responseCode == Activity.RESULT_CANCELED) {
                    // Dialog was cancelled (user pressed back key, for instance). In our game,
                    // this means leaving the room too. In more elaborate games, this could mean
                    // something else (like minimizing the waiting room UI).
                    Log.d(TAG, "Result cancelled.");
                    leaveRoom();
                }
                break;
            case RC_SIGN_IN:
                Log.d(TAG, "onActivityResult with requestCode == RC_SIGN_IN, responseCode="
                        + responseCode + ", intent=" + intent);
                mSignInClicked = false;
                mResolvingConnectionFailure = false;
                if (responseCode == RESULT_OK) {
                    mGoogleApiClient.connect();
                } else {
                    BaseGameUtils.showActivityResultError(this, requestCode, responseCode, R.string.signin_other_error);
                }
                break;
        }
        super.onActivityResult(requestCode, responseCode, intent);
    }

    @Override
    public void onRoomConnecting(Room room) {

    }

    @Override
    public void onRoomAutoMatching(Room room) {

    }

    @Override
    public void onPeerInvitedToRoom(Room room, List<String> list) {
        updateRoom(room);
    }


    // We treat most of the room update callbacks in the same way: we update our list of
    // participants and update the display. In a real game we would also have to check if that
    // change requires some action like removing the corresponding player avatar from the screen,
    // etc.
    @Override
    public void onPeerDeclined(Room room, List<String> list) {
        updateRoom(room);
        // peer declined invitation -- see if game should be canceled
        if (!mPlaying && shouldCancelGame(room)) {
            Games.RealTimeMultiplayer.leave(mGoogleApiClient, null, mRoomId);
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        }
    }

    @Override
    public void onPeerJoined(Room room, List<String> list) {

    }

    @Override
    public void onPeerLeft(Room room, List<String> list) {
        updateRoom(room);
        // peer left -- see if game should be canceled
        if (!mPlaying && shouldCancelGame(room)) {
            Games.RealTimeMultiplayer.leave(mGoogleApiClient, null, mRoomId);
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        }
    }

    @Override
    public void onConnectedToRoom(Room room) {
        Log.d(TAG, "onConnectedToRoom.");

        //get participants and my ID:
        mParticipants = room.getParticipants();
        mMyId = room.getParticipantId(Games.Players.getCurrentPlayerId(mGoogleApiClient));

        // save room ID if its not initialized in onRoomCreated() so we can leave cleanly before the game starts.
        if (mRoomId == null)
            mRoomId = room.getRoomId();

        // print out the list of participants (for debug purposes)
        Log.d(TAG, "Room ID: " + mRoomId);
        Log.d(TAG, "My ID " + mMyId);
        Log.d(TAG, "<< CONNECTED TO ROOM>>");
    }


    // Called when we get disconnected from the room. We return to the main screen.
    @Override
    public void onDisconnectedFromRoom(Room room) {
        mRoomId = null;
        showGameError();
    }


    // Show error message about game being cancelled and return to main screen.
    void showGameError() {
        BaseGameUtils.makeSimpleDialog(this, "An error has occurred");
        switchToMainScreen();
    }

    @Override
    public void onPeersConnected(Room room, List<String> list) {

        if (mPlaying) {
            // add new player to an ongoing game
            updateRoom(room);
        } else if (shouldStartGame(room)) {
            // start game!
            startGame();
        }
    }

    @Override
    public void onPeersDisconnected(Room room, List<String> list) {
        updateRoom(room);
        if (mPlaying) {
            // do game-specific handling of this -- remove player's avatar
            // from the screen, etc. If not enough players are left for
            // the game to go on, end the game and leave the room.
        } else if (shouldCancelGame(room)) {
            // cancel the game
            Games.RealTimeMultiplayer.leave(mGoogleApiClient, null, mRoomId);
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        }
    }

    @Override
    public void onP2PConnected(String s) {

    }

    @Override
    public void onP2PDisconnected(String s) {

    }


    // Called when room has been created
    @Override
    public void onRoomCreated(int statusCode, Room room) {
        Log.d(TAG, "onRoomCreated(" + statusCode + ", " + room + ")");
        if (statusCode != GamesStatusCodes.STATUS_OK) {
            Log.e(TAG, "*** Error: onRoomCreated, status " + statusCode);
            showGameError();
            startGame();
        } else {
            // save room ID so we can leave cleanly before the game starts.
            mRoomId = room.getRoomId();

            // show the waiting room UI
            showWaitingRoom(room);
        }


    }

    @Override
    public void onJoinedRoom(int statusCode, Room room) {
        Log.d(TAG, "onJoinedRoom(" + statusCode + ", " + room + ")");
        if (statusCode != GamesStatusCodes.STATUS_OK) {
            Log.e(TAG, "*** Error: onRoomConnected, status " + statusCode);
            showGameError();
            return;
        }

        // show the waiting room UI
        showWaitingRoom(room);
    }


    // Called when we've successfully left the room (this happens a result of voluntarily leaving
    // via a call to leaveRoom(). If we get disconnected, we get onDisconnectedFromRoom()).
    @Override
    public void onLeftRoom(int statusCode, String s) {
        // we have left the room; return to main screen.
        Log.d(TAG, "onLeftRoom, code " + statusCode);
        switchToMainScreen();
    }


    // Called when room is fully connected.
    @Override
    public void onRoomConnected(int statusCode, Room room) {
        Log.d(TAG, "onRoomConnected(" + statusCode + ", " + room + ")");
        if (statusCode != GamesStatusCodes.STATUS_OK) {
            Log.e(TAG, "*** Error: onRoomConnected, status " + statusCode);
            showGameError();
            return;
        }
        updateRoom(room);
        showWaitingRoom(room);
    }

    @Override
    public void onRealTimeMessageReceived(RealTimeMessage realTimeMessage) {

    }

    @Override
    public void onInvitationReceived(Invitation invitation) {
        // We got an invitation to play a game! So, store it in
        // mIncomingInvitationId
        // and show the popup on the screen.
        mIncomingInvitationId = invitation.getInvitationId();
//        ((TextView) findViewById(R.id.incoming_invitation_text)).setText(
//                invitation.getInviter().getDisplayName() + " " +
//                        getString(R.string.is_inviting_you));
    }

    @Override
    public void onInvitationRemoved(String invitationId) {
        if (mIncomingInvitationId.equals(invitationId) && mIncomingInvitationId != null) {
            mIncomingInvitationId = null;

        }
    }

    /***************************************************************************
     * *******************Additional code***************************************
     **************************************************************************/
// Call when the sign-in button is clicked
    private void signInClicked() {
        mSignInClicked = true;
        mGoogleApiClient.connect();
    }

    // Call when the sign-out button is clicked
    private void signOutclicked() {
        mSignInClicked = false;
        if (mGoogleApiClient.isConnected()) {
            Games.signOut(mGoogleApiClient);
        }
        switchToScreen(R.id.screen_sign_in);
    }

    // Leave the room.
    void leaveRoom() {
        Log.d(TAG, "Leaving room.");
        stopKeepingScreenOn();
        if (mRoomId != null) {
            Games.RealTimeMultiplayer.leave(mGoogleApiClient, this, mRoomId);
            mRoomId = null;
            switchToScreen(R.id.screen_wait);
        } else {
            switchToMainScreen();
        }
    }

    void switchToMainScreen() {
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            switchToScreen(R.id.screen_main);
        } else {
            switchToScreen(R.id.screen_sign_in);
        }
    }

    void switchToViewPuzzleScreen() {
        Display display = getWindowManager().getDefaultDisplay();
        ImageView previewThumbnail = new ImageView(this);
        previewThumbnail = (ImageView) findViewById(R.id.view_image);
        int swidth = (int) (display.getWidth());
        previewThumbnail.getLayoutParams().width = swidth;

        previewThumbnail.getLayoutParams().height = swidth;
        previewThumbnail.setImageBitmap(imageBitmap);
        //viewFlipper.showNext();
        //mCurScreen = R.id.screen_view_puzzle;
        switchToScreen(R.id.screen_view_puzzle);
    }


    // Clears the flag that keeps the screen on.
    void stopKeepingScreenOn() {
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    // Sets the flag to keep this screen on. It's recommended to do that during
    // the
    // handshake when setting up a game, because if the screen turns off, the
    // game will be
    // cancelled.
    void keepScreenOn() {
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    // Handle back key to make sure we cleanly leave a game if we are in the middle of one
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent e) {
        if (keyCode == KeyEvent.KEYCODE_BACK && (mCurScreen == R.id
                .screen_main || mCurScreen == R.id.screen_sign_in)) {
            leaveRoom();
            return true;
        }
        return super.onKeyDown(keyCode, e);
    }


    // Show the waiting room UI to track the progress of other players as they enter the
    // room and get connected.
    void showWaitingRoom(Room room) {
        // minimum number of players required for our game
        // For simplicity, we require everyone to join the game before we start it
        // (this is signaled by Integer.MAX_VALUE)
        Intent i = Games.RealTimeMultiplayer.getWaitingRoomIntent(mGoogleApiClient, room, MIN_PLAYERS);
        Log.w(TAG,
                "Waiting Room");
        // show waiting room UI
        startActivityForResult(i, RC_WAITING_ROOM);
    }


    // Accept the given invitation.
    void acceptInviteToRoom(String invId) {
        // accept the invitation
        Log.d(TAG, "Accepting invitation: " + invId);
        RoomConfig.Builder roomConfigBuilder = RoomConfig.builder(this);
        roomConfigBuilder.setInvitationIdToAccept(invId)
                .setMessageReceivedListener(this)
                .setRoomStatusUpdateListener(this);
        switchToScreen(R.id.screen_wait);
        keepScreenOn();
        Games.RealTimeMultiplayer.join(mGoogleApiClient, roomConfigBuilder.build());
        switchToScreen(R.id.game_screen);
    }


    void updateRoom(Room room) {
        if (room != null) {
            mParticipants = room.getParticipants();
        }
        if (mParticipants != null) {
            //updatePeerScoresDisplay();
        }
    }

    // returns whether there are enough players to start the game
    boolean shouldStartGame(Room room) {
        Log.w(TAG,
                "Should start game");
        int connectedPlayers = 0;
        for (Participant p : room.getParticipants()) {
            if (p.isConnectedToRoom()) ++connectedPlayers;
        }
        return connectedPlayers >= MIN_PLAYERS;
    }

    // Returns whether the room is in a state where the game should be canceled.
    boolean shouldCancelGame(Room room) {
        // TODO: Your game-specific cancellation logic here. For example, you might decide to
        // cancel the game if enough people have declined the invitation or left the room.
        // You can check a participant's status with Participant.getStatus().
        // (Also, your UI should have a Cancel button that cancels the game too)
        Log.w(TAG,
                "Should cancel game");
        int connectedPlayers = 0;
        for (Participant p : room.getParticipants()) {
            if (p.isConnectedToRoom()) ++connectedPlayers;
        }
        return connectedPlayers < MIN_PLAYERS;
    }


    /**************************************************************************
     * ***************************GAMEPLAY CODE**********************************
     **************************************************************************/

    private ImageView image1, image2, image3, image4, image5, image6, image7, image8, image9;
    private GestureDetector gestureDetector;
    public int count = 0, switchCounter = 0;

    String KEY_CHALLENGE_TEMP = "puzzleTemp";

    //images of the PicSwitch
    Bitmap img, img0, img1, img2, img3, img4, img5, img6, img7, img8;
    ArrayList<Integer> images = new ArrayList<Integer>(Arrays.asList(0, 1, 2, 3,
            4,5, 6, 7, 8));
    int chunkNumbers = 9;
    ImageView placeholder;

    int animationToRun = R.anim.fadeout; //animation to be used when user touches PicSwitch

    Bitmap imageBitmap;
    View.OnTouchListener onTouchListener;


    public void imageSetupHelper() {
        Random rand = new Random();
        int photoNumber = rand.nextInt(24);
        //download image from backendless
//        Backendless.Persistence.of( PhotoInfo.class).findFirst( new
//                                                                AsyncCallback<PhotoInfo>(){
//            @Override
//            public void handleResponse( PhotoInfo contact )
//            {
//                // first contact instance has been found
//            }
//            @Override
//            public void handleFault( BackendlessFault fault )
//            {
//                // an error has occurred, the error code can be retrieved with fault.getCode()
//            }
//        });
        String url = "https://api.backendless" +
                ".com/723238F7-E6F7-1228-FF28-0259B6672700/v1/files/media" +
                "/test/" + photoNumber + ".jpg";
        loadImageFromServer(url);

//        splitImage(imageBitmap, chunkNumbers);
//        createImageFromBitmap(imageBitmap, KEY_CHALLENGE_TEMP); //save image to disk for viewing in next intent
//        imageViewsHelper(); //initializes image views
//        imageViewsDimensions(); //sets up the dimensions of the imageviews
//        assignImageViewPics(); //add images to imageViews
//        setImageViewTouch();
    }

    /**
     * Creates images from the bitmaps
     */
    public String createImageFromBitmap(Bitmap bitmap, String name) {
        String fileName = name;//no .png or .jpg needed
        try {
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 25, bytes);
            FileOutputStream fo = openFileOutput(fileName, Context.MODE_PRIVATE);
            fo.write(bytes.toByteArray());
            // remember close file output
            fo.close();
        } catch (Exception e) {
            e.printStackTrace();
            fileName = null;
        }
        return fileName;

    }

    /**
     * Sets touch detection for imageViews
     */
    public void setImageViewTouch() {
        //set touch for imageviews
        gestureDetector = new GestureDetector(this, new MyGestureDetector());
        onTouchListener = new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                return gestureDetector.onTouchEvent(event);
            }
        };

        image1.setOnTouchListener(onTouchListener);
        image2.setOnTouchListener(onTouchListener);
        image3.setOnTouchListener(onTouchListener);
        image4.setOnTouchListener(onTouchListener);
        image5.setOnTouchListener(onTouchListener);
        image6.setOnTouchListener(onTouchListener);
        image7.setOnTouchListener(onTouchListener);
        image8.setOnTouchListener(onTouchListener);
        image9.setOnTouchListener(onTouchListener);
    }


    /**
     * Assigns bitmaps from the SD to image Views
     */
    public void assignImageViewPics() {
        //shuffle images arraylist
        //		Collections.shuffle(images);

        //		ArrayList<Integer> eightStep = new ArrayList<Integer>();
        //
        //		//first step, shuffle the array
        //		Collections.shuffle(images);
        //
        //		for(int i = 0; i < 9; i++){
        //			eightStep.add(i, (images.get(i) + 1) % 9);
        //		}
        //
        //		System.out.println("eight step: " + eightStep.toString());

        ImageView previewThumbnail = new ImageView(this);
        int[] ids = {R.id.imageView01, R.id.imageView02, R.id.imageView03, R.id.imageView04, R.id.imageView05, R.id.imageView06, R.id.imageView07, R.id.imageView08, R.id.imageView09};
        for (int i = 0; i < 9; i++) {
            previewThumbnail = (ImageView) findViewById(ids[i]);
            Bitmap bitmap;
            bitmap = chunkedImages.get(images.get(i)); //get image using images array that has been shuffled
            previewThumbnail.setImageBitmap(bitmap);
        }
    }

    /**
     * Initializes the dimensions of the imageviews that hold the chunked
     * images
     */
    public void imageViewsDimensions() {
        Display display = getWindowManager().getDefaultDisplay();
        //width of the imageViews
        //minus 6 to allows for space between imageViews
        int swidth = (int) (display.getWidth() / 3 - 6);
        image1.getLayoutParams().width = swidth;
        image1.getLayoutParams().height = swidth;
        image2.getLayoutParams().width = swidth;
        image2.getLayoutParams().height = swidth;
        image3.getLayoutParams().width = swidth;
        image3.getLayoutParams().height = swidth;
        image4.getLayoutParams().width = swidth;
        image4.getLayoutParams().height = swidth;
        image5.getLayoutParams().width = swidth;
        image5.getLayoutParams().height = swidth;
        image6.getLayoutParams().width = swidth;
        image6.getLayoutParams().height = swidth;
        image7.getLayoutParams().width = swidth;
        image7.getLayoutParams().height = swidth;
        image8.getLayoutParams().width = swidth;
        image8.getLayoutParams().height = swidth;
        image9.getLayoutParams().width = swidth;
        image9.getLayoutParams().height = swidth;
    }


    /**
     * Initialize imageViews to hold chucked images
     */
    public void imageViewsHelper() {
        image1 = (ImageView) findViewById(R.id.imageView01);
        image2 = (ImageView) findViewById(R.id.imageView02);
        image3 = (ImageView) findViewById(R.id.imageView03);
        image4 = (ImageView) findViewById(R.id.imageView04);
        image5 = (ImageView) findViewById(R.id.imageView05);
        image6 = (ImageView) findViewById(R.id.imageView06);
        image7 = (ImageView) findViewById(R.id.imageView07);
        image8 = (ImageView) findViewById(R.id.imageView08);
        image9 = (ImageView) findViewById(R.id.imageView09);
    }

    /**
     * Detects touch and starts timer
     */
    class MyGestureDetector extends GestureDetector.SimpleOnGestureListener {
        int timerCount = 0;

        @Override
        public boolean onDown(MotionEvent e) {
            count++;
            onTap(e);
            return false;
        }

    }

    /**
     * Helper to swap images on imageViews
     */
    private void swapImages(ImageView initial, ImageView current, int init,
                            int finImage, int coordinates) {
        int tmp, tmp2;

        try {
            //swap the images in initial and current imageViews
            //bitmap = BitmapFactory.decodeStream(this.openFileInput("img"+images.get(in)));
//            initial.setImageBitmap(BitmapFactory.decodeStream(this.openFileInput("img" + fin)));
//            current.setImageBitmap(BitmapFactory.decodeStream(this.openFileInput("img" + in)));

            ImageViewAnimatedChange(this, initial, coordinates, BitmapFactory
            .decodeStream
                    (this
                    .openFileInput("img" + fin)));
            ImageViewAnimatedChange(this, current, coordinates, BitmapFactory
                    .decodeStream
                    (this
                            .openFileInput("img" + in)));
            //swap the numbers in arrayList for in and fin
            tmp = init;
            tmp2 = images.indexOf(finImage);
            images.set(images.indexOf(init), finImage);
            images.set(tmp2, tmp);
            checkFinish(initial, current);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }



    /**
     * Checks whether the PicSwitch is solved
     */
    private void checkFinish(ImageView initial, ImageView current) {
        if (
                images.get(0) == 0 &&
                images.get(1) == 1 &&
                images.get(2) == 2 &&
                images.get(3) == 3 &&
                images.get(4) == 4 &&
                images.get(5) == 5 &&
                images.get(6) == 6 &&
                images.get(7) == 7 &&
                images.get(8) == 8) {
            initial.clearAnimation();
            current.clearAnimation();
            done();

        }
    }

    /**
     * Handles the case where the user completes the puzzle
     */
    public void done() {

        switchToViewPuzzleScreen();
    }

    ImageView temp = null;
    int in = 100, fin = 100;

    /**
     * Sets animation when user makes first touch
     */
    public void setAnimation(ImageView im, int i) {
        Animation animation = AnimationUtils.loadAnimation(this, animationToRun);
        im.startAnimation(animation);
        im.setPadding(8, 8, 8, 8);
        im.setBackgroundColor(Color.WHITE);
        temp = im;
        in = i;
    }

    public void secondTouch(ImageView imageView, int i) {
        count = 0;
        if (!(imageView.equals(temp))) {
            switchCounter++;
        }
        temp.setPadding(0, 0, 0, 0);
        temp.clearAnimation();
        fin = i;
        swapImages(temp, imageView, in, fin, getAnimationCoordinates(Math.min
                (in, fin)) - getAnimationCoordinates(Math.max(in, fin)));
    }

    /**
     * Detects touches on imageViews
     * Shows the necessary animations
     * Calls swap images to swap the images
     */
    private void onTap(MotionEvent event) {
        int firstRow = getLocation(image1)[1] + image1.getHeight(); //lower y coordinate of row 1

        //first column
        if (image1.getLeft() < event.getRawX() && image1.getLeft() + image1.getHeight() > event.getRawX()) {
            //image1
            if (image1.getTop() < event.getRawY() && getLocation(image1)[1] + image1.getHeight() > event.getRawY()) {
                if (count == 1) {
                    setAnimation(image1, images.get(0));
                } else if (count == 2) {
                    secondTouch(image1, images.get(0));
                }
            } else if (image4.getTop() < event.getRawY() && getLocation(image4)[1] + image4.getHeight() > event.getRawY()) {
                if (count == 1) {
                    setAnimation(image4, images.get(3));
                } else if (count == 2) {
                    secondTouch(image4, images.get(3));
                }
            } else if (image7.getTop() < event.getRawY() && getLocation(image7)[1] + image7.getHeight() > event.getRawY()) {
                if (count == 1) {
                    setAnimation(image7, images.get(6));
                } else if (count == 2) {
                    secondTouch(image7, images.get(6));
                }
            }

        }
        //second column
        if (image2.getLeft() < event.getRawX() && image2.getLeft() + image2.getHeight() > event.getRawX()) {
            //image1
            if (image2.getTop() < event.getRawY() && firstRow > event.getRawY()) {
                if (count == 1) {
                    setAnimation(image2, images.get(1));
                } else if (count == 2) {
                    secondTouch(image2, images.get(1));
                }
            } else if (image5.getTop() < event.getRawY() && getLocation(image5)[1] + image5.getHeight() > event.getRawY()) {
                if (count == 1) {
                    setAnimation(image5, images.get(4));
                } else if (count == 2) {
                    secondTouch(image5, images.get(4));
                }
            } else if (image8.getTop() < event.getRawY() && getLocation(image8)[1] + image8.getHeight() > event.getRawY()) {
                if (count == 1) {
                    setAnimation(image8, images.get(7));
                } else if (count == 2) {
                    secondTouch(image8, images.get(7));
                }
            }

        }
        //third column
        if (image3.getLeft() < event.getRawX() && image3.getLeft() + image3.getHeight() > event.getRawX()) {
            //image1
            if (image3.getTop() < event.getRawY() && firstRow > event.getRawY()) {
                if (count == 1) {
                    setAnimation(image3, images.get(2));
                } else if (count == 2) {
                    secondTouch(image3, images.get(2));
                }
            } else if (image6.getTop() < event.getRawY() && getLocation(image6)[1] + image6.getHeight() > event.getRawY()) {
                if (count == 1) {
                    setAnimation(image6, images.get(5));
                } else if (count == 2) {
                    secondTouch(image6, images.get(5));
                }
            } else if (image9.getTop() < event.getRawY() && getLocation(image9)[1] + image9.getHeight() > event.getRawY()) {
                if (count == 1) {
                    setAnimation(image9, images.get(8));
                } else if (count == 2) {
                    secondTouch(image9, images.get(8));
                }
            }

        }
    }

    private int[] getLocation(View mView) {
        Rect mRect = new Rect();
        int[] location = new int[2];

        mView.getLocationOnScreen(location);

        mRect.left = location[0];
        mRect.top = location[1];
        mRect.right = location[0] + mView.getWidth();
        mRect.bottom = location[1] + mView.getHeight();

        return location;
    }

    ArrayList<Bitmap> chunkedImages; //saves the chunked images

    /**
     * method to split the image in imageView
     */
    private void splitImage(Bitmap b, int chunkNumbers) {
        Bitmap bitmap = b;

        //For the number of rows and columns of the grid to be displayed
        int rows, cols;

        //For height and width of the small image chunks
        int chunkHeight, chunkWidth;

        //To store all the small image chunks in bitmap format in this list
        chunkedImages = new ArrayList<Bitmap>(chunkNumbers);

        //Getting the scaled bitmap of the source image
        Bitmap scaledBitmap = Bitmap.createScaledBitmap(bitmap, bitmap.getWidth(), bitmap.getHeight(), true);

        rows = cols = (int) Math.sqrt(chunkNumbers);
        chunkHeight = bitmap.getHeight() / rows;
        chunkWidth = bitmap.getWidth() / cols;


        //xCoord and yCoord are the pixel positions of the image chunks
        int yCoord = 0;
        for (int x = 0; x < rows; x++) {
            int xCoord = 0;
            for (int y = 0; y < cols; y++) {
                chunkedImages.add(Bitmap.createBitmap(scaledBitmap, xCoord, yCoord, chunkWidth, chunkHeight));

                xCoord += chunkWidth;
            }
            yCoord += chunkHeight;
        }
        createImageHelper(chunkedImages);
    }


    /**
     * Saves images to sd and starts next activity
     */
    public void createImageHelper(ArrayList<Bitmap> bitmap) {
        for (int i = 0; i < 9; i++) {
            createImageFromBitmap(bitmap.get(i), "img" + i);
        }
        try {
            img0 = BitmapFactory.decodeStream(this.openFileInput("img0"));
            img1 = BitmapFactory.decodeStream(this.openFileInput("img1"));
            img2 = BitmapFactory.decodeStream(this.openFileInput("img2"));
            img3 = BitmapFactory.decodeStream(this.openFileInput("img3"));
            img4 = BitmapFactory.decodeStream(this.openFileInput("img4"));
            img5 = BitmapFactory.decodeStream(this.openFileInput("img5"));
            img6 = BitmapFactory.decodeStream(this.openFileInput("img6"));
            img7 = BitmapFactory.decodeStream(this.openFileInput("img7"));
            img8 = BitmapFactory.decodeStream(this.openFileInput("img8"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }


    public void loadImageFromServer(final String url) {
        new ImageDownload().execute(url);
    }

    public class ImageDownload extends AsyncTask<String, Void, Bitmap> {

        @Override
        protected Bitmap doInBackground(String... params) {
            final String url = params[0];
            Bitmap b = null;
            try {
                DefaultHttpClient httpclient = new DefaultHttpClient();
                HttpRequestRetryHandler retryHandler = new HttpRequestRetryHandler() {
                    @Override
                    public boolean retryRequest(IOException exception, int executionCount,
                                                HttpContext context) {
// TODO Auto-generated method stub
                        if (executionCount >= 5) {
// Do not retry if over max retry count
                            return false;
                        }
                        if (exception instanceof InterruptedIOException) {
// Timeout
                            return false;
                        }
                        if (exception instanceof UnknownHostException) {
// Unknown host
                            return false;
                        }
                        if (exception instanceof ConnectException) {
// Connection refused
                            return false;
                        }
                        if (exception instanceof SSLException) {
// SSL handshake exception
                            return false;
                        }
                        HttpRequest request = (HttpRequest) context.getAttribute(
                                ExecutionContext.HTTP_REQUEST);
                        boolean idempotent = !(request instanceof HttpEntityEnclosingRequest);
                        if (idempotent) {
// Retry if the request is considered idempotent
                            return true;
                        }
                        return false;
                    }
                };
                httpclient.setHttpRequestRetryHandler(retryHandler);
                HttpGet httppost = new HttpGet(url);
//kirim request n ambil response
                ResponseHandler<byte[]> responseHandler = new ResponseHandler<byte[]>() {
                    @Override
                    public byte[] handleResponse(HttpResponse response)
                            throws ClientProtocolException, IOException {
// TODO Auto-generated method stub
                        if (response.getEntity() != null) {
                            return EntityUtils.toByteArray(response.getEntity());
                        } else {
                            response = new DefaultHttpClient().execute(new HttpGet(url));
                            return EntityUtils.toByteArray(response.getEntity());
                        }
                    }
                };
                byte[] bitmapData = httpclient.execute(httppost, responseHandler);
                Log.d("message", String.valueOf(bitmapData.length) + " \n " + bitmapData.toString());
                BitmapFactory.Options option = new BitmapFactory.Options();
                option.inDither = true;
                option.inPurgeable = true;
                option.inInputShareable = true;
                option.inScaled = true;
                option.inDensity = 0;
                option.inScreenDensity = 0;
                option.inPreferredConfig = Bitmap.Config.RGB_565;
                option.inTargetDensity = 0;
                option.inTempStorage = new byte[2 * 1024];
                b = BitmapFactory.decodeByteArray(bitmapData, 0, bitmapData.length);
                httpclient.getConnectionManager().shutdown();
                httppost.abort();
                return b;
            } catch (MalformedURLException e) {
                System.out.println("error load" + e.getMessage());
            } catch (IOException e1) {
                System.out.println("error load" + e1.getMessage());
            }
            return b;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            switchToScreen(R.id.game_screen);
            if (bitmap != null) {
                imageBitmap = bitmap;
                splitImage(imageBitmap, chunkNumbers);
                createImageFromBitmap(imageBitmap, KEY_CHALLENGE_TEMP); //save image to disk for viewing in next intent
                imageViewsHelper(); //initializes image views
                imageViewsDimensions(); //sets up the dimensions of the imageviews
                assignImageViewPics(); //add images to imageViews
                setImageViewTouch();
            } else {
                System.out.println("error loading bitmap from server");
            }
        }
    }


    /**************************************************************************
    ************************** ANIMATIONS *************************************
     **************************************************************************/

//    private void setupWindowAnimations() {
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            Slide slide = new Slide();
//            slide.setDuration(1000);
//            getWindow().setExitTransition(slide);
//        }
//    }

    public static void ImageViewAnimatedChange(Context c, final ImageView v,
                                               int coordinates, final Bitmap
                                                       new_image) {

        int animation_in = android.R.anim.fade_in;
        int animation_out = android.R.anim.fade_out;
        final Animation anim_in = AnimationUtils.loadAnimation(c, animation_in);
        final Animation anim_out  = AnimationUtils.loadAnimation(c, animation_out);


        anim_in.setFillBefore(false);
        anim_in.setFillEnabled(true);

        anim_out.setFillAfter(true);
        anim_out.setAnimationListener(new Animation.AnimationListener()
        {
            @Override public void onAnimationStart(Animation animation) {
                v.setImageBitmap(new_image);
                anim_in.setAnimationListener(new Animation.AnimationListener() {
                    @Override public void onAnimationStart(Animation animation) {}
                    @Override public void onAnimationRepeat(Animation animation) {}
                    @Override public void onAnimationEnd(Animation animation) {}
                });
                v.startAnimation(anim_in);
            }
            @Override public void onAnimationRepeat(Animation animation) {}
            @Override public void onAnimationEnd(Animation animation)
            {

            }
        });
        v.startAnimation(anim_out);
    }

    public static int imageViewAnimationOutPicker(int imageStep){
     if(Math.abs(imageStep) <3){
         //are in the same row, animate left and right
         if(imageStep<0){
             System.out.println("Animation: " + "slide_out_right");
             return R.anim.anim_slide_out_right;
         }
         else{
             System.out.println("Animation: " + "slide_out+left");
             return R.anim.anim_slide_out_left;
         }

     }else if(Math.abs(imageStep) % 13 ==0 ){
         //imageviews are arranged vertically
         //animate vertically
         if(imageStep<0){
             System.out.println("Animation: " + "slide_out_bottom");
             return R.anim.anim_slide_out_bottom;
         }
         else{
             System.out.println("Animation: " + "slide_out_top");
             return R.anim.anim_slide_out_top;
         }

     }
        else if(Math.abs(imageStep) % 14 ==0 ||
             Math.abs(imageStep)  == 15 ||
             Math.abs(imageStep)  == 27){
         //imageViews diagonal backslash
         if(imageStep<0){
             System.out.println("Animation: " + "slide_out_SE");
             return R.anim.anim_slide_out_southeast;
         }
         else{
             System.out.println("Animation: " + "slide_out_NW");
             return R.anim.anim_slide_out_northwest;
         }
     }
//        else if(Math.abs(imageStep) % 12 ==0 ||
//             Math.abs(imageStep)  == 11 ||
//             Math.abs(imageStep)  == 25){
        else{

         //imageViews diagonal forwardslash
         if(imageStep<0){
             System.out.println("Animation: " + "slide_out_SW");
             return R.anim.anim_slide_out_southwest;
         }
         else{
             System.out.println("Animation: " + "slide_out_NE");
             return R.anim.anim_slide_out_northeast;
         }
     }
       // return R.anim.anim_slide_out_right;
    }

    public static int imageViewAnimationInPicker(int imageStep){
        if(Math.abs(imageStep) <3){
            //are in the same row, animate left and right
            if(imageStep<0){
                System.out.println("Animation: " + "slide_in_left");
                return R.anim.anim_slide_in_left;
            }
            else{
                System.out.println("Animation: " + "slide_in_right");
                return R.anim.anim_slide_in_right;
            }

        }else if(Math.abs(imageStep) % 13 ==0 ){
            //imageviews are arranged vertically
            //animate vertically
            if(imageStep<0){
                System.out.println("Animation: " + "slide_in_top");
                return R.anim.anim_slide_in_top;
            }
            else{
                System.out.println("Animation: " + "slide_in_bottom");
                return R.anim.anim_slide_in_bottom;
            }

        }
        else if(Math.abs(imageStep) % 14 ==0 ||
                Math.abs(imageStep)  == 15 ||
                Math.abs(imageStep)  == 27){
            //imageViews diagonal backslash
            if(imageStep<0){
                System.out.println("Animation: " + "slide_in_northwest");
                return R.anim.anim_slide_in_northwest;
            }
            else{
                System.out.println("Animation: " + "slide_in_southeast");
                return R.anim.anim_slide_in_southeast;
            }
        }
//        else if(Math.abs(imageStep) % 12 ==0 ||
//                Math.abs(imageStep)  == 11 ||
//                Math.abs(imageStep)  == 25){
        else{
            //imageViews diagonal forwardslash
            if(imageStep<0){
                System.out.println("Animation: " + "slide_in_northeast");
                return R.anim.anim_slide_in_northeast;
            }
            else{
                System.out.println("Animation: " + "slide_in_southwest");
                return R.anim.anim_slide_in_southwest;
            }
        }
        //return R.anim.anim_slide_in_left;
    }

    public int getAnimationCoordinates(int index){
        if(index<3){
            System.out.println("Index: " + index+10);
            return index+10;

        }
        else if(index>2 && index < 6){
            System.out.println("Index: " + index+20);
            return index+20;
        }
        else {
            System.out.println("Index: " + index+30);
            return index + 30;
        }
    }

}
