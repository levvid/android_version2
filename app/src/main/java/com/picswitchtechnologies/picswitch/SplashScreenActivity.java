package com.picswitchtechnologies.picswitch;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

public class SplashScreenActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Start MainActivity immediately after splash screen done
        //setupWindowAnimations();
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }


//    private void setupWindowAnimations() {
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            Slide slide = new Slide();
//            slide.setDuration(1000);
//            getWindow().setExitTransition(slide);
//        }
//    }
}
